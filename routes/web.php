<?php

use App\Http\Controllers\TugasController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TugasController@home');
Route::get('/ganjilgenap', 'TugasController@ganjilgenap');
Route::post('/ganjilgenap', 'TugasController@cek');
Route::get('/hitungvokal', 'TugasController@hitungvokal');
Route::post('/hitungvokal', 'TugasController@hasilvokal');
Route::get('/kalkulator', 'TugasController@kalkulator');
Route::post('/kalkulator', 'TugasController@hasilkalkulator');

