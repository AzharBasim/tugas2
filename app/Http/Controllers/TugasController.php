<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ErrorException;

class TugasController extends Controller
{
    public function home()
        {
            return view ('home');
        }


    public function ganjilgenap()
        {
            return view ('ganjilgenap/ganjilgenap');
        }
    public function cek(Request $request)
        {
            if($request['bil1'] === null || $request['bil2'] === null){
             return redirect('/ganjilgenap/ganjilgenap')->with(['warning' => 'Input wajib diisi!']);
        }

            $bil1 = $request['bil1'];
            $bil2 = $request['bil2'];
            $start = $bil1 > $bil2 ? $bil2 : $bil1;
            $end = $bil1 > $bil2 ? $bil1 : $bil2;

            $data = [
                'start' => $start,
                'end' => $end
            ];
            return view('/ganjilgenap/cek', $data);
        }

     public function hitungvokal()
        {
            return view ('/hitungvokal/hitungvokal');
        }

    public function hasilvokal(Request $request)
        {
            if($request['words'] === null){
            return redirect('/hitungvokal/hasilvokal')->with(['warning' => 'Input Wajib Diisi!']);
            }
            $words = explode(" ", htmlspecialchars($request['words']));
            $vocal = ['a', 'i', 'u', 'e', 'o'];
            $results = [];

            foreach ($words as $word){
                $arr = str_split($word);
                $found = array_intersect($vocal, $arr);
                $hasil = array_unique($found);


                array_push($results, "$word = ".count($hasil) ." yaitu ".implode(", ",$hasil));
            }

            $data = [
                'array' => $results
            ];

            return view('/hitungvokal/hasilvokal', $data);
        }
    public function kalkulator()
        {
            return view ('/kalkulator');
        }
    public function hasilkalkulator(Request $request)
        {
            if ($request['bil1'] === null || $request['bil2'] === null)
            {
                return redirect('/kalkulator')->with(['warning' => 'Input Bilangan Harus Diisi!']);
            }

            if($request['bil2'] === '0' && $request['operation'] == '/')
            {
                return redirect('/kalkulator')->with(['warning' => 'Angka Tidak Bisa Dibagi Dengan 0']);
            }

            $data = [
                'bil1' => $request['bil1'],
                'operation' => $request['operation'],
                'bil2' => $request['bil2']
            ];

            $calc = implode(" ", $data);
            $hitung = eval('return '.$calc.';');
            $hasil = $hitung;

            return redirect('/kalkulator')->with(['success' => 'hasil operasi: '.$hasil]);
    }
}
