@extends('layout/main')

@section('title', 'Kalkulator')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">

            <h1>Kalkulator</h1>
            <form action="/kalkulator" method="POST">
                @csrf
                <div class="form-group">
                    <label for="bil1">Input angka pertama</label>
                    <input type="number" name="bil1" class="form-control" id="bil1">
                </div>
                <div class="form-group">
                    <label for="bil2">Input angka kedua</label>
                    <input type="number" name="bil2" class="form-control" id="bil2">
                </div>
                <button type="submit" name="operation" class="btn btn-info" value="-">-</button>
                <button type="submit" name="operation" class="btn btn-info" value="+">+</button>
                <button type="submit" name="operation" class="btn btn-info" value="*">*</button>
                <button type="submit" name="operation" class="btn btn-info" value="/">/</button>
            </form>

            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-15">
                <strong>{{ $message }}</strong>
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-danger alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
        </div>
    </div>
</div>

@endsection
