@extends('layout/main')

@section('title', 'Ganjil Genap')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Hitung Ganjil Genap</h1>

            @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
	        </div>
	        @endif

            <form action="/ganjilgenap" method="POST">
                @csrf
                <label for="bil1">Input A</label>
                <input type="number" name="bil1">
                <br>
                <label for="bil2">Input B</label>
                <input type="number" name="bil2">
                <br>
                <button type="submit" class="btn btn-warning">submit</button>

        </div>
    </div>
</div>

@endsection
