@extends('layout/main')

@section('title', 'Ganjil Genap')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Cetak Ganjil Genap</h1>

            <table class="table table-bordered table-sm">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Hasil</th>
                    </tr>
                </thead>
                <tbody>
                    @for ($i = $start; $i <= $end; $i++)
                        @if ($i % 2 === 0)
                            <tr class="table-primary">
                                <td>{{ $i }} adalah bilangan genap</td>
                            </tr>
                            @else
                            <tr class="table-success">
                                <td>{{ $i }} adalah bilangan ganjil</td>
                            </tr>
                        @endif
                    @endfor
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
