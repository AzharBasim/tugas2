@extends('layout/main')

@section('title', 'Hitung Vokal')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Hitung Vokal</h1>
            @if ($message = Session::get('warning'))
                <div class="alert alert-warning alert-block mt-5">
                    <button type="button" class="close" data-dismiss="alert">tutup</button>
                    <strong>{{ $message }}</strong>
                </div>
	        @endif

            <form action="/hitungvokal" method="POST">
                @csrf
                <div class="form-group">
                    <label for="bil1">Masukan kalimat yang akan dihitung huruf vokalnya</label>
                    <textarea name="words" id="words" class="form-control"></textarea>
                </div>
                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection
